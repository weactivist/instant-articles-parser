'use strict';

let Parser = require('rss-parser');
let parser = new Parser();
let sha1 = require('sha1');
let mongoose = require('mongoose');
let InstantArticleFeed = require('./schema/InstantArticleFeed');
let InstantArticle = require('./schema/InstantArticle');

mongoose.connect(process.env.MONGODB_URI).catch(function(error) {
    console.log('Connection error: ', error);
});

InstantArticleFeed.find({}).exec().then(function(feeds) {
    for( var i = 0; i < feeds.length; i++ ) {
        (async () => {
            let feed = feeds[i];
            let rss = await parser.parseURL(feed.link);

            Promise.all(rss.items.map(function(item) {
                return new Promise(function(resolve, reject) {
                    let guid = item.guid ? item.guid : item.link;
                    let id = guid ? sha1(guid) : '';

                    if(id) {
                        InstantArticle.findById(id).exec().then(function(article) {
                            if( !article ) {
                                article = new InstantArticle();
                                article._id = id;
                                article.link = item.link;
                            }

                            article.title = item.title;
                            article.description = item.description;
                            article.author = item.author;
                            article.guid = item.guid;
                            article.pubDate = item.pubDate;
                            article.content = item['content:encoded'];
                            article.status = 'waiting';

                            article.save(function(error, article) {
                                if(error) {
                                    reject(error);
                                }
                                else {
                                    resolve(article);
                                }
                            });
                        }).catch(function(error) {
                            reject(error);
                        });
                    }
                    else {
                        resolve();
                    }
                });
            })).then(function(articles) {
                console.log('SUCCESS! ', rss.title);
                mongoose.connection.close();
            }).catch(function(error) {
                console.log('ERROR: ', rss.title, error);
                mongoose.connection.close();
            });
        })();
    }
}).catch(function(error) {
    console.log(error);
    mongoose.connection.close();
});
